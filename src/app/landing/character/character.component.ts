import {Component, Input, OnInit} from '@angular/core';
import {CharacterService} from 'src/app/landing/character/character.service';
import {Character} from 'src/app/shared/dtos/character';
import {FormControl} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  @Input() character: Character;
  userid: string;
  classes: Map<string, number>;
  backstory = new FormControl();
  personalityTraits = new FormControl();
  ideals = new FormControl();
  bonds = new FormControl();
  languages: string[];

  constructor(private characterService: CharacterService, private authService: AuthService) {
  }

  ngOnInit() {
    this.userid = this.authService.user.value.userid;
    this.classes = new Map(Object.entries(this.character.classes));
  }

  getCharacter() {
    this.characterService.getCharacter(this.character.characterName, this.userid).subscribe(character => {
      this.character = character;
    });
  }

}

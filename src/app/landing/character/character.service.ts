import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Character} from '../../shared/dtos/character';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private httpClient: HttpClient) {
  }

  getCharacter(characterName: string, userid: string): Observable<Character> {
    return this.httpClient.get<Character>(`${environment.servicesUrl}/api/character/get?characterName=${characterName}&userid=${userid}`)
      .pipe(map(result => result));
  }

  updateCharacter(character: Character) {
    return this.httpClient.put<Character>(
      `${environment.servicesUrl}/api/character/update`, character)
      .pipe(map(result => result));
  }

  addCharacter(character: Character): Observable<Character> {
    return this.httpClient.post<Character>(`${environment.servicesUrl}/api/character/add`, character)
      .pipe(map(results => results));
  }
}

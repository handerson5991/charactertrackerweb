import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Inventory} from '../shared/dtos/inventory';
import {Character} from '../shared/dtos/character';
import {SpellsAndAbilities} from '../shared/dtos/spellsAndAbilities';
import {CharacterService} from './character/character.service';
import {InventoryService} from './inventory/inventory.service';
import {SpellsAndAbilitiesService} from './spells-and-abilities/spells-and-abilities.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  characterName: string;
  userid: string;
  character: Character;
  inventory: Inventory;
  spellsAndAbilities: SpellsAndAbilities;
  loading = false;

  constructor(private route: ActivatedRoute, private characterService: CharacterService, private authService: AuthService,
              private inventoryService: InventoryService, private spellsAndAbilitiesService: SpellsAndAbilitiesService) {
  }

  ngOnInit() {
    this.userid = this.authService.user.value.userid;
    this.loading = true;
    this.route.queryParams.subscribe(params => {
      this.characterName = params.character;
      this.characterService.getCharacter(this.characterName, this.userid).subscribe(characterResult => {
        this.character = characterResult;
        this.inventoryService.getInventory(this.characterName, this.userid).subscribe(invResult => {
          this.inventory = invResult;
          this.spellsAndAbilitiesService.getSpellsAndAbilities(this.characterName, this.userid).subscribe(spellAbilityResult => {
            this.spellsAndAbilities = spellAbilityResult;
            this.loading = false;
          })
        })
      })
    });
  }
}

import {Component, OnInit} from '@angular/core';
import {faAngleLeft} from '@fortawesome/free-solid-svg-icons/faAngleLeft';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons/faAngleRight';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  icon = faAngleLeft;
  isOpen = false;

  constructor() {
  }

  ngOnInit() {
  }

  changeIcon() {
    this.icon = !this.isOpen ? faAngleLeft : faAngleRight;
  }
}

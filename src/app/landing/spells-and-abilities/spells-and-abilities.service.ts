import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {SpellsAndAbilities} from '../../shared/dtos/spellsAndAbilities';

@Injectable({
  providedIn: 'root'
})
export class SpellsAndAbilitiesService {

  constructor(private httpClient: HttpClient) {
  }

  getSpellsAndAbilities(characterName: string, userid: string): Observable<SpellsAndAbilities> {
    return this.httpClient.get<SpellsAndAbilities>(
      `${environment.servicesUrl}/api/spellsAndAbilities/get?characterName=${characterName}&userid=${userid}`)
      .pipe(map(results => results));
  }

  addSpellsAndAbilities(spellsAndAbilities: SpellsAndAbilities): Observable<SpellsAndAbilities> {
    return this.httpClient.post<SpellsAndAbilities>(`${environment.servicesUrl}/api/spellsAndAbilities/add`, spellsAndAbilities)
      .pipe(map(results => results));
  }

  updateSpellsAndAbilities(spellsAndAbilities: SpellsAndAbilities): Observable<SpellsAndAbilities> {
    return this.httpClient.put<SpellsAndAbilities>(`${environment.servicesUrl}/api/spellsAndAbilities/update`, spellsAndAbilities)
      .pipe(map(results => results));
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Ability} from '../../../shared/dtos/ability';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons/faPencilAlt';
import {faAngleDown} from '@fortawesome/free-solid-svg-icons/faAngleDown';
import {faAngleUp} from '@fortawesome/free-solid-svg-icons';
import {modalOptions} from '../../../shared/config';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AbilityModalComponent} from '../ability-modal/ability-modal.component';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-ability',
  templateUrl: './ability.component.html',
  styleUrls: ['./ability.component.css'],
  animations: [
    trigger('slideUpDown', [
      transition(':enter', [
        style({opacity: 0, transform: 'translateY(-20%)'}),
        animate('300ms cubic-bezier(.4, 0, .2, 1)', style({opacity: 1, transform: 'translateY(0)'}))
      ])
    ]),
  ]
})
export class AbilityComponent implements OnInit {
  @Input() abilities: Ability[];
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faPencil = faPencilAlt;
  selectedAbilityIndex: number;
  @Output() editedAbility: EventEmitter<Ability> = new EventEmitter(null);
  @Output() deletedAbility: EventEmitter<Ability> = new EventEmitter(null);

  constructor(private modalService: NgbModal) {
  }

  ngOnInit(): void {
  }

  showAbility(index: number) {
    this.selectedAbilityIndex = index !== this.selectedAbilityIndex ? index : -1;
  }

  editAbility(index: number) {
    const ability = this.abilities[index];
    const abilityModal = this.modalService.open(AbilityModalComponent, modalOptions);
    abilityModal.componentInstance.selectedAbility = ability;
    abilityModal.result.then(result => {
      if (result !== 'cancel') {
        if (result === 'delete') {
          this.selectedAbilityIndex = null;
          this.deletedAbility.emit(ability);
         } else {
             this.editedAbility.emit(result);
            }
         }
    }).catch(() => {
    });
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {warningModalOptions} from '../../../shared/config';
import {WarningModalComponent} from '../../../shared/warning-modal/warning-modal.component';
import {Ability} from '../../../shared/dtos/ability';

@Component({
  selector: 'app-ability-modal',
  templateUrl: './ability-modal.component.html',
  styleUrls: ['./ability-modal.component.css']
})
export class AbilityModalComponent implements OnInit {
  @Input() selectedAbility: Ability;
  abilityForm: FormGroup;
  areChanges = false;

  constructor(public activeModal: NgbActiveModal, private form: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.selectedAbility ? this.setForm(this.selectedAbility) :
      this.setForm({
        name: '',
        castingTime: '',
        range: '',
        duration: '',
        description: ''
      });
    this.onChange();
  }

  setForm(selectedAbility: Ability) {
    this.abilityForm = this.form.group({
      name: new FormControl(selectedAbility.name),
      castingTime: new FormControl(selectedAbility.castingTime),
      range: new FormControl(selectedAbility.range),
      duration: new FormControl(selectedAbility.duration),
      description: new FormControl(selectedAbility.description)
    });
  }

  save() {
    return {
      name: this.abilityForm.get('name').value,
      castingTime: this.abilityForm.get('castingTime').value,
      range: this.abilityForm.get('range').value,
      duration: this.abilityForm.get('duration').value,
      description: this.abilityForm.get('description').value
    };
  }

  private onChange() {
    this.abilityForm.valueChanges.subscribe(() => {
      this.areChanges = true;
    });
  }

  delete() {
    const warningModal = this.modalService.open(WarningModalComponent, warningModalOptions);
    warningModal.componentInstance.message = `Are you sure you want to delete '${this.selectedAbility.name}'?`;
    warningModal.result.then(result => {
      if (result === 'continue') {
        this.activeModal.close('delete');
      }
    }).catch(() => {
    });
  }
}

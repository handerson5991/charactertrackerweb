import {Component, Input, OnInit} from '@angular/core';
import {SpellsAndAbilitiesService} from './spells-and-abilities.service';
import {SpellsAndAbilities} from '../../shared/dtos/spellsAndAbilities';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SpellModalComponent} from './spell-modal/spell-modal.component';
import {modalOptions, snackbarSuccessOptions, snackbarWarningOptions} from '../../shared/config';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Ability} from '../../shared/dtos/ability';
import {AbilityModalComponent} from './ability-modal/ability-modal.component';
import {Spell} from '../../shared/dtos/spell';
import {SpellSlots} from '../../shared/dtos/spellSlots';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-spells-and-abilities',
  templateUrl: './spells-and-abilities.component.html',
  styleUrls: ['./spells-and-abilities.component.css'],
  animations: []
})
export class SpellsAndAbilitiesComponent implements OnInit {
  @Input() characterName: string;
  @Input() spellsAndAbilities: SpellsAndAbilities;
  userid: string;
  currentTab = 'spells';
  spells: Spell[];
  abilities: Ability[];
  spellSlots: SpellSlots[];

  constructor(private spellsAndAbilitiesService: SpellsAndAbilitiesService, private modalService: NgbModal,
              private snackbar: MatSnackBar, private authService: AuthService) {
  }

  ngOnInit() {
    this.userid = this.authService.user.value.userid;
    this.spells = this.spellsAndAbilities.spells;
    this.spellSlots = this.spellsAndAbilities.spellSlots;
    this.abilities = this.spellsAndAbilities.abilities;
  }

  setSpellsAndAbilities() {
    this.spellsAndAbilitiesService.getSpellsAndAbilities(this.characterName, this.userid).subscribe(result => {
      if (result) {
        this.spellsAndAbilities = result;
        this.spells = result.spells;
        this.spellSlots = result.spellSlots;
        this.abilities = result.abilities;
      }
    });
  }

  updateSpellsAndAbilities(type: string, action, reset: boolean) {
    this.spellsAndAbilitiesService.updateSpellsAndAbilities(this.spellsAndAbilities).subscribe(() => {
      if (reset) {
        this.setSpellsAndAbilities();
        this.snackbar.open(`${type} ${action}ed`, '', snackbarSuccessOptions);
      }
    }, error => {
      console.log(error);
      if (reset) {
        this.snackbar.open(`There was an issue ${action}ing the ${type}`, '', snackbarWarningOptions);
      }
    });
  }

  changeTab($event) {
    this.currentTab = $event.index === 0 ? 'spells' : 'abilities';
  }

  // ------------------------------------------Spells------------------------------------------

  addSpell() {
    const spellModal = this.modalService.open(SpellModalComponent, modalOptions);
    spellModal.result.then(result => {
      if (result !== 'cancel' && result !== 'delete') {
        this.spellsAndAbilities.spells.push(result);
        this.updateSpellsAndAbilities('Spell', 'add', true);
      }
    }).catch(() => {
    });
  }

  editSpell(editedSpell) {
    this.spellsAndAbilities.spells =
      this.spellsAndAbilities.spells.filter(spell => spell.name !== editedSpell.name);
    this.spellsAndAbilities.spells.push(editedSpell);
    this.updateSpellsAndAbilities('Spell', 'edit', true);
  }

  deleteSpell(deletedSpell) {
    const index = this.spellsAndAbilities.spells.indexOf(deletedSpell);
    this.spellsAndAbilities.spells.splice(index, 1);
    this.updateSpellsAndAbilities('Spell', 'delet', true);
  }

  updateSlots(slots) {
    this.spellSlots = slots;
    this.spellsAndAbilities.spellSlots = this.spellSlots;
    this.updateSpellsAndAbilities('', '', false);
  }

  // ------------------------------------------Abilities------------------------------------------

  addAbility() {
    const abilityModal = this.modalService.open(AbilityModalComponent, modalOptions);
    abilityModal.result.then(result => {
      if (result !== 'cancel' && result !== 'delete') {
        this.spellsAndAbilities.abilities.push(result);
        this.updateSpellsAndAbilities('Ability', 'add', true);
      }
    }).catch(() => {
    });
  }

  editAbility(editedAbility) {
    this.spellsAndAbilities.abilities =
      this.spellsAndAbilities.abilities.filter(ability => ability.name !== editedAbility.name);
    this.spellsAndAbilities.abilities.push(editedAbility);
    this.updateSpellsAndAbilities('Ability', 'edit', true);
  }

  deleteAbility(deletedAbility) {
    const index = this.spellsAndAbilities.abilities.indexOf(deletedAbility);
    this.spellsAndAbilities.abilities.splice(index, 1);
    this.updateSpellsAndAbilities('Ability', 'delet', true);
  }
}

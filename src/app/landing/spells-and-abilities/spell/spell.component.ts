import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faAngleDown, faAngleUp} from '@fortawesome/free-solid-svg-icons';
import {animate, style, transition, trigger} from '@angular/animations';
import {Spell} from '../../../shared/dtos/spell';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons/faPencilAlt';
import {SpellModalComponent} from '../spell-modal/spell-modal.component';
import {modalOptions} from '../../../shared/config';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SpellSlots} from '../../../shared/dtos/spellSlots';

@Component({
  selector: 'app-spell',
  templateUrl: './spell.component.html',
  styleUrls: ['./spell.component.css'],
  animations: [
    trigger('slideUpDown', [
      transition(':enter', [
        style({opacity: 0, transform: 'translateY(-20%)'}),
        animate('300ms cubic-bezier(.4, 0, .2, 1)', style({opacity: 1, transform: 'translateY(0)'}))
      ])
    ]),
  ]
})
export class SpellComponent implements OnInit {
  @Input() spells: Spell[];
  @Input() spellSlots: SpellSlots[];
  @Input() characterCreate: boolean;
  @Output() editedSpell: EventEmitter<Spell> = new EventEmitter(null);
  @Output() deletedSpell: EventEmitter<Spell> = new EventEmitter(null);
  @Output() slotEmitter: EventEmitter<SpellSlots[]> = new EventEmitter(null);
  @Output() addedSpell: EventEmitter<Spell> = new EventEmitter(null);
  levelList = [];
  numSlots: Map<number, number[]>;
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faPencil = faPencilAlt;
  selectedSpellIndex: number;

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
    this.spells.forEach(spell => {
      if (!this.levelList.includes(spell.level)) {
        this.levelList.push(spell.level);
        this.levelList = this.levelList.sort((a, b) => a > b ? 1 : -1);
      }
    });

    if (!this.characterCreate) {
      this.numSlots = new Map();
      this.levelList.forEach(lvl => {
        if (lvl !== 0) {
          this.numSlots.set(lvl, this.slots(lvl));
        }
      });
    }
  }

  showSpell(index: number) {
    this.selectedSpellIndex = index !== this.selectedSpellIndex ? index : -1;
  }

  addSpell(spell: Spell) {
    this.addedSpell.emit(spell);
  }

  editSpell(index: number) {
    const spell = this.spells[index];
    const spellModal = this.modalService.open(SpellModalComponent, modalOptions);
    spellModal.componentInstance.selectedSpell = spell;
    spellModal.result.then(result => {
      if (result !== 'cancel') {
        if (result === 'delete') {
          this.selectedSpellIndex = null;
          this.deletedSpell.emit(spell);
        } else {
          this.editedSpell.emit(result);
        }
      }
    }).catch(() => {
    });
  }

  slots(lvl: number) {
    const currentSlot = this.spellSlots.filter(a => a.level === lvl)[0];
    const array1 = Array(currentSlot.slotTotals.used).fill(1);
    const array2 = Array(currentSlot.slotTotals.total - currentSlot.slotTotals.used).fill(0);
    return array1.concat(array2);
  }

  slotToggle(toggle: boolean, lvl: number) {
    const currentSlot = this.spellSlots.filter(a => a.level === lvl)[0];
    currentSlot.slotTotals.used = toggle ? currentSlot.slotTotals.used + 1 : currentSlot.slotTotals.used - 1;
    this.spellSlots = this.spellSlots.filter(a => a.level !== lvl);
    this.spellSlots.push(currentSlot);
    this.numSlots.set(lvl, this.slots(lvl));
    this.slotEmitter.emit(this.spellSlots);
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Spell} from '../../../shared/dtos/spell';
import {warningModalOptions} from '../../../shared/config';
import {WarningModalComponent} from '../../../shared/warning-modal/warning-modal.component';
import {levels, schools} from '../../../shared/Lists';

@Component({
  selector: 'app-spell-modal',
  templateUrl: './spell-modal.component.html',
  styleUrls: ['./spell-modal.component.css']
})
export class SpellModalComponent implements OnInit {
  @Input() selectedSpell: Spell;
  schools = schools;
  levels = levels;
  spellForm: FormGroup;
  areChanges = false;

  constructor(public activeModal: NgbActiveModal, private form: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.selectedSpell ? this.setForm(this.selectedSpell) :
      this.setForm({
        name: '',
        level: 0,
        school: '',
        casting_time: '',
        range: '',
        duration: '',
        description: '',
        type: '',
        classes: [],
        tags: [],
        ritual: false,
        components: null
      });
    this.onChange();
  }

  setForm(selectedSpell: Spell) {
    this.spellForm = this.form.group({
      name: new FormControl(selectedSpell.name),
      level: new FormControl(selectedSpell.level),
      school: new FormControl(selectedSpell.school),
      castingTime: new FormControl(selectedSpell.casting_time),
      range: new FormControl(selectedSpell.range),
      duration: new FormControl(selectedSpell.duration),
      description: new FormControl(selectedSpell.description)
    });
  }

  selectLvl(level: string) {
    this.spellForm.get('level').setValue(level);
  }

  selectSchool(school: string) {
    this.spellForm.get('school').setValue(school);
  }

  save() {
    return {
      name: this.spellForm.get('name').value,
      level: this.spellForm.get('level').value,
      school: this.spellForm.get('school').value,
      castingTime: this.spellForm.get('castingTime').value,
      range: this.spellForm.get('range').value,
      duration: this.spellForm.get('duration').value,
      description: this.spellForm.get('description').value
    };
  }

  private onChange() {
    this.spellForm.valueChanges.subscribe(() => {
      this.areChanges = true;
    });
  }

  delete() {
    const warningModal = this.modalService.open(WarningModalComponent, warningModalOptions);
    warningModal.componentInstance.message = `Are you sure you want to delete '${this.selectedSpell.name}'?`;
    warningModal.result.then(result => {
      if (result === 'continue') {
        this.activeModal.close('delete');
      }
    }).catch(() => {
    });
  }
}

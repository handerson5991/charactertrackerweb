import {Component, Input, OnInit} from '@angular/core';
import {CharacterService} from 'src/app/landing/character/character.service';
import {Character} from 'src/app/shared/dtos/character';
import {charSkills, dexSkills, intSkills, strSkills, wisSkills} from '../../shared/Lists';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  @Input() character: Character;
  baseStats: Map<string, number>;
  abilityModifiers = new Map<string, number>();
  passivePerception: number;
  currentProficiency: number;
  inspiration: number;
  proficiencies: string[];
  skills: Map<string, string[]> = new Map();

  constructor(private characterService: CharacterService) {
  }

  ngOnInit() {
    this.setAttributes();
  }

  setAttributes() {
    this.baseStats = new Map(Object.entries(this.character.stats));
    this.proficiencies = this.character.proficiencies;
    this.currentProficiency = this.character.proficiency;
    this.skills
      .set('Str', strSkills)
      .set('Dex', dexSkills)
      .set('Int', intSkills)
      .set('Wis', wisSkills)
      .set('Cha', charSkills);
    this.getAbilityModifiers();
    this.getPassivePerception();
  }

  updateCharacter(character: Character) {
    this.characterService.updateCharacter(character).subscribe(result => {
      this.character = result;
      this.setAttributes();
    });
  }

  getPassivePerception() {
    if (this.proficiencies.includes('Perception')) {
      this.passivePerception = 10 + this.abilityModifiers.get('Wis') + this.character.proficiency;
    } else {
      this.passivePerception = 10 + this.abilityModifiers.get('Wis');
    }
  }

  getAbilityModifiers() {
    for (const [key, value] of this.baseStats) {
      this.abilityModifiers.set(key, Math.floor(value / 2 - 5));
    }
  }

  editProficiency(value: string) {
    if (!this.proficiencies.includes(value)) {
      this.proficiencies.push(value);
    } else {
      this.proficiencies.splice(this.proficiencies.indexOf(value), 1);
    }
    this.character.proficiencies = this.proficiencies;
    this.updateCharacter(this.character);
  }

  updateStat(stat: string, value: number) {
    this.baseStats[stat] = +value;
    this.character.stats = this.baseStats;
    this.updateCharacter(this.character);
  }

  updateProficiency(value: number) {
    this.currentProficiency = +value;
    this.character.proficiency = this.currentProficiency;
    this.updateCharacter(this.character);
  }

  updateInspiration(value: number) {
    this.inspiration = +value;
    this.character.inspiration = this.inspiration;
    this.updateCharacter(this.character);
  }

  updateSpeed(value: number) {
    this.character.speed = +value;
    this.updateCharacter(this.character);
  }

  updateAC(value: number) {
    this.character.armorClass = +value;
    this.updateCharacter(this.character);
  }

  updateHP(value: number) {
    this.character.hp = +value;
    this.updateCharacter(this.character);
  }

  updateCurrentHp(value: number) {
    this.character.currentHp = +value;
    this.updateCharacter(this.character);
  }
}

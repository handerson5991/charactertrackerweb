import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons/faPencilAlt';
import {faAngleDown} from '@fortawesome/free-solid-svg-icons/faAngleDown';
import {faAngleUp} from '@fortawesome/free-solid-svg-icons/faAngleUp';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {InventoryService} from '../inventory.service';
import {Item} from '../../../shared/dtos/item';
import {modalOptions} from '../../../shared/config';
import {ItemModalComponent} from '../item-modal/item-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Spell} from '../../../shared/dtos/spell';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  animations: [
    trigger('slideUpDown', [
      transition(':enter', [
        style({opacity: 0, transform: 'translateY(-20%)'}),
        animate('300ms cubic-bezier(.4, 0, .2, 1)', style({opacity: 1, transform: 'translateY(0)'}))
      ])
    ]),
  ]
})
export class ItemComponent implements OnInit {
  @Input() items: Item[];
  @Input() characterCreate = false;
  @Output() itemEmitter = new EventEmitter<Item[]>();
  @Output() editedItem: EventEmitter<Item> = new EventEmitter(null);
  @Output() deletedItem: EventEmitter<Item> = new EventEmitter(null);
  @Output() addedItem: EventEmitter<Item> = new EventEmitter(null);
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faPencil = faPencilAlt;
  selectedItemIndex: number;
  quantity = new FormControl();

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  showItem(index: number) {
    this.selectedItemIndex = index !== this.selectedItemIndex ? index : -1;
  }

  editItem(index: number) {
    const item = this.items[index];
    const itemModal = this.modalService.open(ItemModalComponent, modalOptions);
    itemModal.componentInstance.selectedItem = item;
    itemModal.result.then(result => {
      if (result !== 'cancel') {
        if (result === 'delete') {
          this.selectedItemIndex = null;
          this.deletedItem.emit(item);
        } else {
          this.editedItem.emit(result);
        }
      }
    }).catch(() => {
    });
  }

  updateQuantity(item: Item, quantity: number) {
    if (item.quantity !== quantity) {
      this.items.splice(this.items.indexOf(item), 1);
      item.quantity = quantity;
      this.items.push(item);
      this.itemEmitter.emit(this.items);
    }
  }

  addItem(item: Item) {
    this.addedItem.emit(item);
  }
}

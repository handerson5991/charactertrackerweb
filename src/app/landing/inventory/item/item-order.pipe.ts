import {Pipe, PipeTransform} from '@angular/core';
import {Item} from '../../../shared/dtos/item';

@Pipe({
  name: 'itemOrder',
  pure: false
})
export class ItemOrderPipe implements PipeTransform {

  transform(fields: Item[]) {
    return fields.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);
  }
}

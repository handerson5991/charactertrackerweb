import {Injectable} from '@angular/core';
import {Inventory} from '../../shared/dtos/inventory';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SpellsAndAbilities} from '../../shared/dtos/spellsAndAbilities';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private httpClient: HttpClient) {
  }

  getInventory(characterName: string, userid: string) {
    return this.httpClient.get<Inventory>(
      `${environment.servicesUrl}/api/inventory/get?characterName=${characterName}&userid=${userid}`)
      .pipe(map(result => result));
  }

  updateInventory(inventory: Inventory) {
    return this.httpClient.put<Inventory>(
      `${environment.servicesUrl}/api/inventory/update`, inventory)
      .pipe(map(result => result));
  }

  addInventory(inventory: Inventory): Observable<Inventory> {
    return this.httpClient.post<Inventory>(`${environment.servicesUrl}/api/inventory/add`, inventory)
      .pipe(map(results => results));
  }
}

import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Inventory} from 'src/app/shared/dtos/inventory';
import {Item} from 'src/app/shared/dtos/item';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {InventoryService} from './inventory.service';
import {modalOptions, snackbarSuccessOptions, snackbarWarningOptions} from '../../shared/config';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ItemModalComponent} from './item-modal/item-modal.component';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class InventoryComponent implements OnInit {
  @Input() characterName: string;
  @Input() inventory: Inventory;
  @Output() focusOut: EventEmitter<any> = new EventEmitter();
  userid: string;
  currentTab = 'items';
  moneyForm: FormGroup;
  items: Item[];
  weapons: Item[];

  constructor(private inventoryService: InventoryService, private form: FormBuilder, private modalService: NgbModal,
              private snackbar: MatSnackBar, private authService: AuthService) {
  }

  ngOnInit() {
    this.userid = this.authService.user.value.userid;
    this.setItems();
    this.setMoneyForm();
  }

  changeTab($event) {
    this.currentTab = $event.index === 0 ? 'items' : 'weapons';
  }

  getInventory() {
    this.inventoryService.getInventory(this.characterName, this.userid).subscribe(inventory => {
      this.inventory = inventory;
      this.setItems();
      this.setMoneyForm();
    });
  }

  setItems() {
    this.items = this.inventory.items.filter(item => !item.type || !item.type.includes('Weapon'));
    this.weapons = this.inventory.items.filter(item => item.type && item.type.includes('Weapon'));
  }

  updateInventory(action: string) {
    this.inventoryService.updateInventory(this.inventory).subscribe(() => {
      this.getInventory();
      this.snackbar.open(`Item ${action}ed`, '', snackbarSuccessOptions);
    }, error => {
      console.log(error);
      this.snackbar.open(`There was an issue ${action}ing the item`, '', snackbarWarningOptions);
    });
  }

  updateItemQuantity($event, type: string) {
    if (type === 'item') {
      this.items = $event;
    } else {
      this.weapons = $event;
    }
    this.inventory.items = this.items.concat(this.weapons);
    this.inventoryService.updateInventory(this.inventory).subscribe(() => {
    });
  }

  addItem() {
    const itemModal = this.modalService.open(ItemModalComponent, modalOptions);
    itemModal.result.then(result => {
      if (result !== 'cancel' && result !== 'delete') {
        this.inventory.items.push(result);
        this.updateInventory('add');
      }
    }).catch(() => {
    });
  }

  editItem(editedItem, type: string) {
    if (type === 'item') {
      this.items = this.items.filter(item => item.name !== editedItem.name);
      this.items.push(editedItem);
    } else {
      this.weapons = this.weapons.filter(weapon => weapon.name !== editedItem.name);
      this.weapons.push(editedItem);
    }
    this.inventory.items = this.items.concat(this.weapons);
    this.updateInventory('edit');
  }

  deleteItem(deletedItem, type: string) {
    if (type === 'item') {
      this.items.splice(this.items.indexOf(deletedItem), 1);
    } else {
      this.weapons.splice(this.weapons.indexOf(deletedItem), 1);
    }
    this.inventory.items = this.items.concat(this.weapons);
    this.updateInventory('delet');
  }

  setMoneyForm() {
    this.moneyForm = this.form.group({
      platinum: new FormControl(this.inventory.platinum),
      gold: new FormControl(this.inventory.gold),
      silver: new FormControl(this.inventory.silver),
      copper: new FormControl(this.inventory.copper)
    });
  }

  updateMoney() {
    if (this.moneyForm.dirty) {
      this.inventory.platinum = this.moneyForm.get('platinum').value;
      this.inventory.gold = this.moneyForm.get('gold').value;
      this.inventory.silver = this.moneyForm.get('silver').value;
      this.inventory.copper = this.moneyForm.get('copper').value;
      this.inventoryService.updateInventory(this.inventory).subscribe(() => {
      });
    }
  }
}

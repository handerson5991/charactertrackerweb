import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WarningModalComponent} from '../../../shared/warning-modal/warning-modal.component';
import {warningModalOptions} from '../../../shared/config';
import {Item} from '../../../shared/dtos/item';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import {damageTypes, properties, itemTypes} from '../../../shared/Lists';

@Component({
  selector: 'app-item-modal',
  templateUrl: './item-modal.component.html',
  styleUrls: ['./item-modal.component.css']
})
export class ItemModalComponent implements OnInit {
  @Input() selectedItem: Item;
  itemForm: FormGroup;
  faTimesCircle = faTimesCircle;
  areChanges = false;
  props = [];
  itemTypes = itemTypes;
  damageTypes = damageTypes;
  properties = properties;

  constructor(public activeModal: NgbActiveModal, private form: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit() {
    if (this.selectedItem) {
      this.setForm(this.selectedItem);
    } else {
      this.setForm({
        name: '',
        quantity: null,
        armorClass: 0,
        weight: null,
        description: '',
        type: '',
        damageType: '',
        damage: '',
        range: '',
        rarity: '',
        properties: []
      });
    }
    this.onChange();
  }

  setForm(selectedItem: Item) {
    this.itemForm = this.form.group({
      name: new FormControl(selectedItem.name),
      description: new FormControl(selectedItem.description),
      quantity: new FormControl(selectedItem.quantity),
      weight: new FormControl(selectedItem.weight),
      type: new FormControl(selectedItem.type),
      damageType: new FormControl(selectedItem.damageType),
      damage: new FormControl(selectedItem.damage),
      range: new FormControl(selectedItem.range),
      rarity: new FormControl(selectedItem.rarity),
      properties: new FormControl(selectedItem.properties)
    });
  }

  private onChange() {
    this.itemForm.valueChanges.subscribe(() => {
      this.areChanges = true;
    });
  }

  selectType(type: string) {
    this.itemForm.get('type').setValue(type);
  }

  selectDamageType(type: string) {
    this.itemForm.get('damageType').setValue(type);
  }

  addChip(prop: string) {
    this.props.push(prop);
    this.itemForm.get('properties').setValue(this.props);
  }

  removeChip(prop: string) {
    this.props.splice(this.props.indexOf(prop), 1);
    this.itemForm.get('properties').setValue(this.props);
  }

  save() {
    return {
      name: this.itemForm.get('name').value,
      description: this.itemForm.get('description').value,
      quantity: this.itemForm.get('quantity').value,
      weight: this.itemForm.get('weight').value,
      type: this.itemForm.get('type').value,
      damageType: this.itemForm.get('damageType').value,
      damage: this.itemForm.get('damage').value,
      range: this.itemForm.get('range').value,
      rarity: this.itemForm.get('rarity').value,
      properties: this.itemForm.get('properties').value
    };
  }

  delete() {
    const warningModal = this.modalService.open(WarningModalComponent, warningModalOptions);
    warningModal.componentInstance.message = `Are you sure you want to delete '${this.selectedItem.name}'?`;
    warningModal.result.then(result => {
      if (result === 'continue') {
        this.activeModal.close('delete');
      }
    }).catch(() => {
    });
  }
}


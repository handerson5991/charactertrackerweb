export interface Ability {
  name: string;
  description: string;
  castingTime: string;
  range: string;
  duration: string;
}

export class Character {
    id: string;
    characterName: string;
    userid: string;
    race: string;
    backStory: string;
    background: string;
    alignment: string;
    personalityTraits: string;
    ideals: string;
    bonds: string;
    flaws: string;
    hitDice: string;
    inspiration: number;
    proficiency: number;
    armorClass: number;
    speed: number;
    hp: number;
    currentHp: number;
    languages: string[];
    proficiencies: string[];
    classes: object;
    stats: object;
    savingThrows: object;
}

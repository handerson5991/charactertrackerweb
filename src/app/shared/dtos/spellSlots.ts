export interface SpellSlots {
  level: number;
  slotTotals: {total, used};
}

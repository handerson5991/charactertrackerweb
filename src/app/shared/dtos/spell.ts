export interface Spell {
  name: string;
  level: number;
  school: string;
  type: string;
  classes: string[];
  tags: string[];
  components: Map<string, string>;
  description: string;
  casting_time: string;
  range: string;
  duration: string;
  ritual: boolean;
}

import {Spell} from './spell';
import {Ability} from './ability';
import {SpellSlots} from './spellSlots';

export interface SpellsAndAbilities {
  id: string;
  characterName: string;
  userid: string;
  spellCastingAbility: string;
  spellSave: string;
  spellAttackBonus: string;
  spellSlots: SpellSlots[];
  spells: Spell[];
  abilities: Ability[];
}

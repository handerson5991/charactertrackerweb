export class Inventory {
id: string;
characterName: string;
userid: string;
platinum: number;
gold: number;
silver: number;
copper: number;
items: any[];
}

export class Item {
  name: string;
  description: string;
  quantity: number;
  weight: number;
  armorClass: number;
  type: string;
  damageType: string;
  damage: string;
  range: string;
  rarity: string;
  properties: string[];
}

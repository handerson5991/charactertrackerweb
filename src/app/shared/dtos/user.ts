export interface User {
  userid: string;
  password: string;
  characters: string[];
}

export const properties = [
  'Ammunition',
  'Finesse',
  'Heavy',
  'Light',
  'Loading',
  'Range',
  'Reach',
  'Special',
  'Thrown',
  'Two-Handed',
  'Versatile',
  'Improvised Weapons',
  'Silvered Weapons',
  'Special Weapons'
];

export const damageTypes = [
  'Slashing',
  'Piercing',
  'Bludgeoning',
  'Poison',
  'Acid',
  'Fire',
  'Cold',
  'Radiant',
  'Necrotic',
  'Lightning',
  'Thunder',
  'Force',
  'Psychic'
];

export const schools = [
  'Abjuration',
  'Conjuration',
  'Divination',
  'Enchantment',
  'Evocation',
  'Illusion',
  'Necromancy',
  'Transmutation'
];

export const classes = [
  'Barbarian',
  'Bard',
  'Cleric',
  'Druid',
  'Fighter',
  'Monk',
  'Paladin',
  'Ranger',
  'Rogue',
  'Sorcerer',
  'Warlock',
  'Wizard'
];

export const races = [
  'Dragonborn',
  'Dwarf',
  'Elf',
  'Gnome',
  'Half-Elf',
  'Half-Orc',
  'Halfling',
  'Human',
  'Tiefling'
];

export const strSkills = ['Athletics'];

export const dexSkills = ['Acrobatics', 'Sleight of Hand', 'Stealth'];

export const intSkills = ['Arcana', 'History', 'Investigation', 'Nature', 'Religion'];

export const wisSkills = ['Animal Handling', 'Insight', 'Medicine', 'Perception', 'Survival'];

export const charSkills = ['Deception', 'Intimidation', 'Performance', 'Persuasion'];

export const itemTypes = ['Simple Melee Weapon', 'Simple Ranged Weapon', 'Martial Melee Weapon', 'Martial Ranged Weapon'];

export const levels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];



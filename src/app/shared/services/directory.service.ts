import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Spell} from '../dtos/spell';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Item} from '../dtos/item';

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private httpClient: HttpClient) { }

  getSpellsByClassAndLevel(_class: string, levels: number[]): Observable<Spell[]> {
    return this.httpClient.post<Spell[]>(`${environment.servicesUrl}/api/directory/getSpells/${_class}`, levels)
      .pipe(map(results => results));
  }

  getItems(items: string[]): Observable<Item[]> {
    return this.httpClient.post<Item[]>(`${environment.servicesUrl}/api/directory/getItems`, items)
      .pipe(map(results => results));
  }
}

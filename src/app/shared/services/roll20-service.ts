import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Roll20Service {

  constructor(private httpClient: HttpClient) {
  }

  getRace(race: string) {
    return this.httpClient.get(`${environment.roll20Url}/${race.toLowerCase()}.json`)
      .pipe(map(results => results));
  }

  getClass(_class: string) {
    return this.httpClient.get(`${environment.roll20Url}/${_class.toLowerCase()}.json`)
      .pipe(map(results => results));
  }
}

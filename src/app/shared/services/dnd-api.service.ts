import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DndApiService {

  constructor(private httpClient: HttpClient) {
  }

  getRace(race: string) {
    return this.httpClient.get(`${environment.dndApiUrl}/races/${race.toLowerCase()}`)
      .pipe(map(results => results));
  }

  getTrait(traitUrl: string) {
    return this.httpClient.get(`https://www.dnd5eapi.co${traitUrl}`)
      .pipe(map(results => results));
  }

  getClassFeature(featureUrl: string) {
    return this.httpClient.get(`https://www.dnd5eapi.co${featureUrl}`)
      .pipe(map(results => results));
  }

  getClassLevel(_class: string, level: number) {
    return this.httpClient.get(`${environment.dndApiUrl}/classes/${_class.toLowerCase()}/levels/${level}`)
      .pipe(map(results => results));
  }

  getClass(_class: string) {
    return this.httpClient.get(`${environment.dndApiUrl}/classes/${_class.toLowerCase()}`)
      .pipe(map(results => results));
  }

  getClassSpellInfo(_class: string) {
    return this.httpClient.get(`${environment.dndApiUrl}/classes/${_class.toLowerCase()}/spellcasting`)
      .pipe(map(results => results));
  }

  getEquipment(equipUrl: string) {
    return this.httpClient.get(`https://www.dnd5eapi.co${equipUrl}`)
      .pipe(map(results => results));
  }
}

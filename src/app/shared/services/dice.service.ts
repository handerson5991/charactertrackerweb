import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DiceService {

  constructor() {
  }

  roll(amount: number, dice: number) {
    let total = 0;
    for (let i = 0; i < amount; i++) {
      total = total + Math.floor(Math.random() * dice + 1);
    }
    return total;
  }

  discardLowest(amount: number, dice: number) {
    const results = [];
    let total = 0;
    for (let i = 0; i < amount; i++) {
      results.push(Math.floor(Math.random() * dice + 1));
    }
    results.sort((a, b) => a - b);
    results.splice(0, 1);
    results.forEach(result => total = total + result);
    return total;
  }
}

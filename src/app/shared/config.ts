import {NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBarConfig } from '@angular/material/snack-bar';

export const modalOptions: NgbModalOptions = {
  centered: true,
  size: 'lg',
  scrollable: true
};

export const warningModalOptions: NgbModalOptions = {
  centered: true,
};

export const snackbarWarningOptions: MatSnackBarConfig = {
  duration: 2500,
  verticalPosition: 'top',
  horizontalPosition: 'center',
  panelClass: 'warning-snackbar'
};

export const snackbarSuccessOptions: MatSnackBarConfig = {
  duration: 2000,
  verticalPosition: 'top',
  horizontalPosition: 'center',
  panelClass: 'success-snackbar'
};

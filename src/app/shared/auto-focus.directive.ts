import {AfterContentInit, Directive, ElementRef, Input} from '@angular/core';


@Directive({
  selector: '[appAutoFocus]'
})
export class AutoFocusDirective implements AfterContentInit {
  @Input() appFocus = false;
  @Input() focusDelay = 0;

  public constructor(private elementRef: ElementRef) {
  }

  public ngAfterContentInit() {
    this.checkFocus();
    setTimeout(() => {
      this.elementRef.nativeElement.focus();
    }, 300);
  }

  private checkFocus() {
    if (this.appFocus && document.activeElement !== this.elementRef.nativeElement) {
      let checkFocusTimeoutHandle: number;
      const focus = () => {
        this.elementRef.nativeElement.focus();
      };
      // Even without a delay, we wait for the next JavaScript tick
      // to avoid causing changes on parent components (e.g., the
      // TextInput component) that have already been checked on this
      // change detection cycle.
      checkFocusTimeoutHandle = setTimeout(focus, this.focusDelay) as any;
    }
  }
}

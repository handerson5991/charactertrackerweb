import {Pipe, PipeTransform} from '@angular/core';
import {Item} from './dtos/item';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {

  transform(fields: string[]) {
    return fields.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
  }
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './landing/navbar/navbar.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CharacterComponent} from './landing/character/character.component';
import {LoginComponent} from './login/login.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InventoryComponent} from './landing/inventory/inventory.component';
import {StatsComponent} from './landing/stats/stats.component';
import {AutoFocusDirective} from './shared/auto-focus.directive';
import {SpellsAndAbilitiesComponent} from './landing/spells-and-abilities/spells-and-abilities.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {LandingComponent} from './landing/landing.component';
import {SpellModalComponent} from './landing/spells-and-abilities/spell-modal/spell-modal.component';
import {WarningModalComponent} from './shared/warning-modal/warning-modal.component';
import {AbilityComponent} from './landing/spells-and-abilities/ability/ability.component';
import {AbilityModalComponent} from './landing/spells-and-abilities/ability-modal/ability-modal.component';
import {ItemComponent} from './landing/inventory/item/item.component';
import {ItemModalComponent} from './landing/inventory/item-modal/item-modal.component';
import {OrderByPipe} from './shared/order-by.pipe';
import {ItemOrderPipe} from './landing/inventory/item/item-order.pipe';
import {SpellComponent} from './landing/spells-and-abilities/spell/spell.component';
import {CharacterSelectComponent} from './character-select/character-select.component';
import {NewCharacterModalComponent} from './character-select/new-character-modal/new-character-modal.component';
import {BypassHtmlPipe} from './shared/bypass-html-pipe';
import {AbilityScoresComponent} from './character-select/new-character-modal/ability-scores/ability-scores.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SpellSelectComponent} from './character-select/new-character-modal/spell-select/spell-select.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSpinnerOverlayComponent} from './shared/mat-spinner-overlay/mat-spinner-overlay.component';
import {ItemSelectComponent} from './character-select/new-character-modal/item-select/item-select.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CharacterComponent,
    LoginComponent,
    InventoryComponent,
    StatsComponent,
    AutoFocusDirective,
    SpellsAndAbilitiesComponent,
    SpellComponent,
    LandingComponent,
    SpellModalComponent,
    WarningModalComponent,
    AbilityComponent,
    AbilityModalComponent,
    ItemComponent,
    ItemModalComponent,
    OrderByPipe,
    ItemOrderPipe,
    CharacterSelectComponent,
    NewCharacterModalComponent,
    BypassHtmlPipe,
    AbilityScoresComponent,
    SpellSelectComponent,
    MatSpinnerOverlayComponent,
    ItemSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    NgbModule,
    MatExpansionModule,
    MatChipsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    FontAwesomeModule,
    MatTabsModule,
    MatSnackBarModule,
    MatChipsModule,
    MatSidenavModule,
    DragDropModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {faUser} from '@fortawesome/free-solid-svg-icons/faUser';
import {faKey} from '@fortawesome/free-solid-svg-icons/faKey';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../auth/auth.service';
import {snackbarSuccessOptions} from '../shared/config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser = faUser;
  faKey = faKey;
  form: FormGroup;
  loading: boolean;
  createToggle = false;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      userid: new FormControl(),
      password: new FormControl()
    });
  }

  login() {
    this.loading = true;
    this.authService.login(this.form.value).subscribe(() => {
      this.loading = false;
    }, () => {
      const passwordInput = document.getElementById('password') as HTMLInputElement;
      passwordInput.select();
    });
  }

  createAccount() {
    this.authService.createAccount(this.form.value).subscribe(() => {
    });
  }

  toggleCreate() {
    this.createToggle = !this.createToggle;
    this.form.get('userid').setValue('');
    this.form.get('password').setValue('');
  }
}

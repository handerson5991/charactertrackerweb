import {NgModule} from '@angular/core';
import {ExtraOptions, PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LandingComponent} from './landing/landing.component';
import {CharacterSelectComponent} from './character-select/character-select.component';
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'character-select', component: CharacterSelectComponent, canActivate: [AuthGuard]},
  {path: 'landing', component: LandingComponent, canActivate: [AuthGuard]}
];

const options: ExtraOptions = {
  preloadingStrategy: PreloadAllModules,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, options)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

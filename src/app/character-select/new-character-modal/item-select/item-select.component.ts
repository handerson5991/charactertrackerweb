import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SpellsAndAbilitiesService} from '../../../landing/spells-and-abilities/spells-and-abilities.service';
import {DndApiService} from '../../../shared/services/dnd-api.service';
import {Item} from '../../../shared/dtos/item';
import {DirectoryService} from '../../../shared/services/directory.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {snackbarSuccessOptions} from '../../../shared/config';

@Component({
  selector: 'app-item-select',
  templateUrl: './item-select.component.html',
  styleUrls: ['./item-select.component.css']
})
export class ItemSelectComponent implements OnInit {
  @Input() equipUrl: string;
  @Output() itemEmitter: EventEmitter<Item[]> = new EventEmitter(null);
  addedItems: Item[] = [];
  selectedItems: Item[] = [];
  choices: Map<string, object[]> = new Map<string, object[]>();
  numChoices = 0;
  tabIndex = 0;

  constructor(private spellsAndAbilitiesService: SpellsAndAbilitiesService, private dndApiService: DndApiService,
              private directoryService: DirectoryService, private snackbar: MatSnackBar) {
  }

  ngOnInit() {
    this.getEquipment();
  }

  getEquipment() {
    this.dndApiService.getEquipment(this.equipUrl).subscribe(result => {
      const startEquip = result['starting_equipment'];
      const choicesToMake = result['choices_to_make'];
      let quantityMap: Map<string, number> = new Map<string, number>();

      startEquip.forEach(item => {
        const quantity = item.item.quantity && item.item.quantity > 0 ? item.item.quantity : 1;
        quantityMap.set(this.fixName(item.item.name), quantity);
      });
      this.directoryService.getItems(Array.from(quantityMap.keys())).subscribe(items => {
        items.forEach(item => item.quantity = quantityMap.get(item.name));
        this.addedItems = this.addedItems.concat(items);
      });

      for (let i = 1; i <= choicesToMake; i++) {
        quantityMap = new Map<string, number>();
        const choiceArr: object[] = [];
        const choiceObj = result[`choice_${i}`];
        choiceObj.forEach(choice => {
          this.numChoices = this.numChoices + choice.choose;
          const fromArr = choice.from;
          fromArr.forEach(item => {
            const quantity = item.item.quantity && item.item.quantity > 0 ? item.item.quantity : 1;
            quantityMap.set(this.fixName(item.item.name), quantity);
          });
          this.directoryService.getItems(Array.from(quantityMap.keys())).subscribe(items => {
            items.forEach(item => item.quantity = quantityMap.get(item.name));
            choiceArr.push({choose: choice.choose, items});
            this.choices.set(`Choice ${i}`, choiceArr);
          });
        });
      }
    });
  }

  fixName(name: string) {
    if (name.includes(',')) {
      const arr = name.split(', ');
      return arr[1] + ' ' + arr[0];
    } else if (name === 'arrow') {
      return 'Arrows';
    } else if (name === 'crossbow bolt') {
      return 'Crossbow Bolts';
    } else if (name === 'leather') {
      return 'Leather Armor';
    } else if (name === 'hide') {
      return 'Hide Armor';
    } else if (name === 'padded') {
      return 'Padded Armor';
    } else if (name === 'plate') {
      return 'Plate Armor';
    } else if (name === 'studded leather') {
      return 'Studded Leather Armor';
    } else {
      return name;
    }
  }

  addItem(event) {
    this.selectedItems.push(event);
  }

  saveItems() {
    this.addedItems = this.addedItems.concat(this.selectedItems);
    this.itemEmitter.emit(this.addedItems);
    this.snackbar.open('Items saved', '', snackbarSuccessOptions)
  }
}

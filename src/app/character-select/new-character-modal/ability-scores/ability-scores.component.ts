import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DiceService} from '../../../shared/services/dice.service';
import {CdkDrag, CdkDragDrop, CdkDropList, transferArrayItem} from '@angular/cdk/drag-drop';
import {faCaretUp} from '@fortawesome/free-solid-svg-icons/faCaretUp';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons/faCaretDown';

@Component({
  selector: 'app-ability-scores',
  templateUrl: './ability-scores.component.html',
  styleUrls: ['./ability-scores.component.css']
})
export class AbilityScoresComponent implements OnInit {
  faCaretUp = faCaretUp;
  faCaretDown = faCaretDown;
  baseStats: Map<string, number>;
  newBaseStats: Map<string, number>;
  tableAbilities = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
  tablePoints = [0, 1, 2, 3, 4, 5, 6, 8, 10, 13, 18];
  method: string;
  pointPool: number;
  rollList = [];
  str: number[];
  dex: number[];
  int: number[];
  con: number[];
  wis: number[];
  cha: number[];

  @Output() statEmitter = new EventEmitter<Map<string, number>>();

  @Input('baseStats') set stats(baseStats: Map<string, number>) {
    this.newBaseStats = baseStats;
    this.baseStats = baseStats;
  }

  constructor(private diceService: DiceService) {
  }

  ngOnInit() {
  }

  calculateAbilities(method: string) {
    this.method = method;
    if (method !== 'Difficulty') {
      this.str = [this.baseStats.get('Str')];
      this.dex = [this.baseStats.get('Dex')];
      this.int = [this.baseStats.get('Int')];
      this.con = [this.baseStats.get('Con')];
      this.wis = [this.baseStats.get('Wis')];
      this.cha = [this.baseStats.get('Cha')];
    }
    if (method === 'Standard') {
      this.standard();
    } else if (method === 'Discard') {
      this.discardLowest(4, 6);
    }
  }

  standard() {
    this.rollList = [15, 14, 13, 12, 10, 8];
  }

  discardLowest(numDice: number, dice: number) {
    this.rollList = [];
    for (let i = 0; i < 6; i++) {
      this.rollList.push(this.diceService.discardLowest(numDice, dice));
    }
  }

  buy(pool: number) {
    this.method = 'Buy';
    this.pointPool = pool;
    this.str = [this.baseStats.get('Str') + 8];
    this.dex = [this.baseStats.get('Dex') + 8];
    this.int = [this.baseStats.get('Int') + 8];
    this.con = [this.baseStats.get('Con') + 8];
    this.wis = [this.baseStats.get('Wis') + 8];
    this.cha = [this.baseStats.get('Cha') + 8];
  }

  addPointFromPool(ability: string, add: boolean) {
    switch (ability) {
      case 'str':
        this.pointPool = this.pointPool - this.getStatCost(this.str[0], add);
        this.str[0] = add ? this.str[0] + 1 : this.str[0] - 1;
        break;
      case 'dex':
        this.pointPool = this.pointPool - this.getStatCost(this.dex[0], add);
        this.dex[0] = add ? this.dex[0] + 1 : this.dex[0] - 1;
        break;
      case 'int':
        this.pointPool = this.pointPool - this.getStatCost(this.int[0], add);
        this.int[0] = add ? this.int[0] + 1 : this.int[0] - 1;
        break;
      case 'con':
        this.pointPool = this.pointPool - this.getStatCost(this.con[0], add);
        this.con[0] = add ? this.con[0] + 1 : this.con[0] - 1;
        break;
      case 'wis':
        this.pointPool = this.pointPool - this.getStatCost(this.wis[0], add);
        this.wis[0] = add ? this.wis[0] + 1 : this.wis[0] - 1;
        break;
      case 'cha':
        this.pointPool = this.pointPool - this.getStatCost(this.cha[0], add);
        this.cha[0] = add ? this.cha[0] + 1 : this.cha[0] - 1;
        break;
    }
    if (this.pointPool === 0) {
      this.saveRolls();
    }
  }

  getStatCost(stat: number, add: boolean) {
    if (stat === 14) {
      return add ? 2 : -1;
    } else if (stat === 15) {
      return add ? 2 : -2;
    } else if (stat === 16) {
      return add ? 3 : -2;
    } else if (stat === 17) {
      return add ? 5 : -3;
    } else if (stat > 17) {
      return add ? 5 : -5;
    }
    return add ? 1 : -1;
  }

  disabled(ability: string, add: boolean) {
    let statValue = 0;
    switch (ability) {
      case 'str':
        statValue = this.str[0];
        break;
      case 'dex':
        statValue = this.dex[0];
        break;
      case 'int':
        statValue = this.int[0];
        break;
      case 'con':
        statValue = this.con[0];
        break;
      case 'wis':
        statValue = this.wis[0];
        break;
      case 'cha':
        statValue = this.cha[0];
        break;
    }
    if (add) {
      return this.pointPool - this.getStatCost(statValue, add) < 0;
    } else {
      return statValue === 0;
    }
  }

  drop(event: CdkDragDrop<number[]>) {
    const currentVal = event.container.data[0];
    transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    event.container.data[0] = event.container.data[0] + currentVal;
    event.container.disabled = true;
    if (this.rollList.length === 0) {
      this.saveRolls();
    }
  }

  hasValuePredicate(drag: CdkDrag<number>, drop: CdkDropList<number[]>) {
    return !drop.disabled;
  }

  saveRolls() {
    this.newBaseStats
      .set('Str', this.str[0])
      .set('Dex', this.dex[0])
      .set('Int', this.int[0])
      .set('Con', this.con[0])
      .set('Wis', this.wis[0])
      .set('Cha', this.cha[0]);
    this.statEmitter.emit(this.newBaseStats);
  }
}

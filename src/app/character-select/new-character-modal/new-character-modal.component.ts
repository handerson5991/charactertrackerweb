import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {classes, races} from '../../shared/Lists';
import {DndApiService} from '../../shared/services/dnd-api.service';
import {Roll20Service} from '../../shared/services/roll20-service';
import {Character} from '../../shared/dtos/character';
import {Inventory} from '../../shared/dtos/inventory';
import {SpellsAndAbilities} from '../../shared/dtos/spellsAndAbilities';
import {MatSnackBar} from '@angular/material/snack-bar';
import {animate, style, transition, trigger} from '@angular/animations';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import {faExclamationTriangle} from '@fortawesome/free-solid-svg-icons/faExclamationTriangle';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {CharacterService} from '../../landing/character/character.service';
import {InventoryService} from '../../landing/inventory/inventory.service';
import {SpellsAndAbilitiesService} from '../../landing/spells-and-abilities/spells-and-abilities.service';
import {snackbarSuccessOptions} from '../../shared/config';
import {AuthService} from '../../auth/auth.service';
import {DirectoryService} from '../../shared/services/directory.service';
import {Item} from '../../shared/dtos/item';

@Component({
  selector: 'app-item-modal',
  templateUrl: './new-character-modal.component.html',
  styleUrls: ['./new-character-modal.component.css'],
  animations: [
    trigger('fadeIn', [
      transition('* => *', [
        style({opacity: 0}),
        animate('0.5s cubic-bezier(.25, .8, .25, 1)', style({opacity: 1}))
      ])
    ])
  ]
})
export class NewCharacterModalComponent implements OnInit {
  faTimesCircle = faTimesCircle;
  faExclamationTriangle = faExclamationTriangle;
  userid: string;
  loading = false;
  numTabs = 7;
  tabIndex = 0;
  characterForm: FormGroup;
  races = races;
  classes = classes;
  selectedRace: object;
  selectedClass: object;
  raceDescription: string;
  classDescription: string;
  cantrips: number;
  spellsKnown: number;
  spellSlots: number;
  spells = [];
  abilities = [];
  languageOptions: object;
  profOptions = [];
  profMaps = [];
  proficiencies = [];
  selectedProfs: Map<number, string[]>;
  savingThrows = [];
  castingAbility: string;
  baseStats = new Map()
    .set('Str', 0)
    .set('Dex', 0)
    .set('Int', 0)
    .set('Con', 0)
    .set('Wis', 0)
    .set('Cha', 0);
  equipUrl: string;
  items: Item[];
  character: Character;
  inventory: Inventory;
  spellsAndAbilities: SpellsAndAbilities;

  constructor(public activeModal: NgbActiveModal, private form: FormBuilder, private modalService: NgbModal,
              private dndApiService: DndApiService, private roll20Service: Roll20Service, private characterService: CharacterService,
              private inventoryService: InventoryService, private spellsAndAbilitiesService: SpellsAndAbilitiesService,
              private snackbar: MatSnackBar, private authService: AuthService) {
  }

  ngOnInit() {
    this.userid = this.authService.user.value.userid;
    this.characterForm = this.form.group({
      characterName: new FormControl(''),
      race: new FormControl(''),
      class: new FormControl(''),
      backstory: new FormControl(''),
      background: new FormControl(''),
      alignment: new FormControl(''),
      personalityTraits: new FormControl(''),
      ideals: new FormControl(''),
      bonds: new FormControl(''),
      flaws: new FormControl(''),
      languages: new FormControl([]),
    });
    this.character = {
      id: null, characterName: null, userid: null, race: null, backStory: null, background: null, alignment: null, personalityTraits: null,
      ideals: null, bonds: null, flaws: null, hitDice: null, inspiration: null, proficiency: null, armorClass: null,
      speed: null, hp: null, currentHp: null, languages: null, proficiencies: null, classes: null, stats: null, savingThrows: null
    };
  }

  changeTab(direction: string) {
    this.tabIndex = direction === 'Next' ? this.tabIndex + 1 : this.tabIndex - 1;
  }

  onTabChange($event: MatTabChangeEvent) {
    switch ($event.index) {
      case this.numTabs - 1:
        this.buildCharacter();
        break;
      case 2:
        this.setRaceInfo();
        break;
      case 3:
        this.setClassInfo();
        break;
      case 4:
        if (this.characterForm.get('class').value.toString().toLowerCase() === 'druid') {
          this.spellsKnown = Math.floor(1 + (this.baseStats.get('Wis') / 2 - 5)) >= 0 ?
            Math.floor(1 + (this.baseStats.get('Wis') / 2 - 5)) : 0;
        }
        break;
    }
  }

  selectRace(race: string) {
    this.characterForm.get('race').setValue(race);
    this.roll20Service.getRace(race).subscribe(result => {
      this.raceDescription = result['htmlcontent'];
      this.dndApiService.getRace(race).subscribe(result => {
        this.selectedRace = result;
        this.languageOptions = this.selectedRace['language_options'] ? this.selectedRace['language_options'] : null;
      });
    });
  }

  setRaceInfo() {
    const languageArray = this.selectedRace['languages'];
    const traitArray = this.selectedRace['traits'];
    const profArray = this.selectedRace['starting_proficiencies'];
    const abilityArray = this.selectedRace['ability_bonuses'];
    languageArray.forEach(language => {
      if (!this.characterForm.get('languages').value) {
        this.characterForm.get('languages').setValue([]);
      }
      if (!this.characterForm.get('languages').value.includes(language.name)) {
        this.characterForm.get('languages').value.push(language.name);
      }
    });
    profArray.forEach(prof => {
      if (!this.proficiencies.includes(prof.name.slice(7))) {
        if (prof.name.includes('Skill')) {
          if (!this.proficiencies.includes(prof.name.slice(7))) {
            this.proficiencies.push(prof.name.slice(7));
          }
        }
      }
    });
    traitArray.forEach(trait => {
      if (!this.abilities.includes(trait.name)) {
        this.dndApiService.getTrait(trait.url).subscribe(result => {
          this.abilities.push({
            name: trait.name, description: result['desc'][0], abilityDice: null, abilityModified: null,
            castingTime: null, damage: null, damageType: null, duration: null, range: null
          });
        });
      }
    });
    abilityArray.forEach(ability => {
      const name = ability.name.charAt(0) + ability.name.slice(1).toLowerCase();
      this.baseStats.set(name, ability.bonus);
    });
  }

  selectClass(_class: string) {
    this.selectedProfs = new Map();
    this.profMaps = [];
    this.characterForm.get('class').setValue(_class);
    this.roll20Service.getClass(_class).subscribe(result => {
      const profArray = result['proficienies'];
      if (profArray && profArray.length > 0) {
        profArray.foreEach(prof => {
          this.proficiencies.push(prof.name);
        });
      }
      this.classDescription = result['htmlcontent'];
    });
    this.dndApiService.getClass(_class).subscribe(result => {
      this.selectedClass = result;
      this.equipUrl = result['starting_equipment']['url'];
      this.profOptions = this.selectedClass['proficiency_choices'];
      this.setProfOptions();
    });
  }

  setProfOptions() {
    this.profOptions.forEach(option => {
      const from = [];
      let choose: number;
      choose = option['choose'];
      option['from'].forEach(prof => {
        const name = prof.name.includes('Skill') ? prof.name.slice(7) : prof.name;
        from.push(name);
      });
      const map = new Map().set(choose, from);
      this.profMaps.push(map);
    });
  }

  setClassInfo() {
    this.dndApiService.getClassLevel(this.characterForm.get('class').value, 1).subscribe(result => {
      const featureArray = result['features'];
      if (result['spellcasting']) {
        this.dndApiService.getClassSpellInfo(this.characterForm.get('class').value).subscribe(results => {
          const cast = results['spellcasting_ability']['name'];
          this.castingAbility = cast.charAt(0) + cast.slice(1).toLowerCase();
        });
        if (this.characterForm.get('class').value.toString().toLowerCase() === 'wizard') {
          this.cantrips = 3;
          this.spellsKnown = 6;
        } else if (this.characterForm.get('class').value.toString().toLowerCase() === 'druid') {
          this.cantrips = 2;
        } else {
          this.cantrips = result['spellcasting']['cantrips_known'];
          this.spellsKnown = result['spellcasting']['spells_known'];
        }
        this.spellSlots = result['spellcasting']['spell_slots_level_1'];
      }
      featureArray.forEach(feature => {
        if (!this.abilities.includes(feature.name)) {
          this.dndApiService.getClassFeature(feature.url).subscribe(results => {
            this.abilities.push({
              name: feature.name, description: results['desc'][0], abilityDice: null, abilityModified: null,
              castingTime: null, damage: null, damageType: null, duration: null, range: null
            });
          });
        }
      });
    });
    const savingThrowArray = this.selectedClass['saving_throws'];
    savingThrowArray.forEach(savingThrow => {
      this.savingThrows.push(savingThrow.name.charAt(0) + savingThrow.name.slice(1).toLowerCase());
    });
  }

  selectLanguage(language: string) {
    if (!this.characterForm.get('languages').value.includes(language)) {
      this.characterForm.get('languages').value.push(language);
    }
  }

  selectProf(prof: string, profChoiceList: string[], i: number) {
    profChoiceList.splice(profChoiceList.indexOf(prof), 1);
    const currentProfs = this.selectedProfs.get(i) !== undefined ? this.selectedProfs.get(i) : [];
    currentProfs.push(prof);
    this.selectedProfs.set(i, currentProfs);
    this.proficiencies.push(prof);
    this.profMaps[i].set(this.profMaps[i].keys().next().value, profChoiceList);
  }

  removeProfChip(prof: string, profChoiceList: string[], i: number) {
    profChoiceList.push(prof);
    profChoiceList.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
    const currentProfs = this.selectedProfs.get(i);
    currentProfs.splice(currentProfs.indexOf(prof), 1);
    this.selectedProfs.set(i, currentProfs);
    this.proficiencies.splice(this.proficiencies.indexOf(prof), 1);
    this.profMaps[i].set(this.profMaps[i].keys().next().value, profChoiceList);
  }

  removeLangChip(lang: string) {
    const formLangs = this.characterForm.get('languages').value;
    formLangs.splice(formLangs.indexOf(lang), 1);
  }

  saveStats(event) {
    this.baseStats = event;
  }

  saveSpells(event) {
    this.spells = event;
  }

  saveItems(event) {
    this.items = event;
  }

  buildCharacter() {
    this.setCharacterInfo();
    this.setInventoryInfo();
    this.setSpellsAndAbilitiesInfo();
    console.log(this.character);
    console.log(this.spellsAndAbilities);
    console.log(this.inventory);
  }

  createCharacter() {
    this.loading = true;
    this.characterService.addCharacter(this.character).subscribe(() => {
      this.inventoryService.addInventory(this.inventory).subscribe(() => {
        this.spellsAndAbilitiesService.addSpellsAndAbilities(this.spellsAndAbilities).subscribe(() => {
          this.loading = false;
          this.snackbar.open('Character created', '', snackbarSuccessOptions);
          this.activeModal.close(this.character.characterName);
        });
      });
    });
  }

  setCharacterInfo() {
    const statMap = {};
    const savingThrowsMap = {};
    const classMap = {};

    let armorClass = 0;
    this.items.forEach(item => {
      switch (item.name) {
        case 'Chain Mail':
          armorClass = 16;
          break;
        case 'Plate':
          armorClass = 18;
          break;
        case 'Shield':
          armorClass = armorClass + 2;
          break;
        default:
          if (item.armorClass && item.armorClass > 0) {
            armorClass = item.armorClass + Math.floor(this.baseStats.get('Dex') / 2 - 5);
          }
      }
    });
    if (armorClass === 0) {
      armorClass = 10 + Math.floor(this.baseStats.get('Dex') / 2 - 5);
    }
    this.baseStats.forEach((val: string, key: string) => {
      statMap[key] = val;
    });
    this.savingThrows.forEach(savingThrow => {
      savingThrowsMap[savingThrow] = Math.floor(this.baseStats.get(savingThrow) / 2 - 5);
    });
    classMap[this.characterForm.get('class').value] = 1;
    this.character.characterName = this.characterForm.get('characterName').value;
    this.character.userid = this.userid;
    this.character.race = this.characterForm.get('race').value;
    this.character.backStory = this.characterForm.get('backstory').value;
    this.character.background = this.characterForm.get('background').value;
    this.character.alignment = this.characterForm.get('alignment').value;
    this.character.personalityTraits = this.characterForm.get('personalityTraits').value;
    this.character.ideals = this.characterForm.get('ideals').value;
    this.character.bonds = this.characterForm.get('bonds').value;
    this.character.flaws = this.characterForm.get('flaws').value;
    this.character.hitDice = `1D${this.selectedClass['hit_die']}`;
    this.character.armorClass = armorClass;
    this.character.proficiency = 2;
    this.character.speed = this.selectedRace['speed'];
    this.character.hp = Math.floor(this.selectedClass['hit_die'] + (this.baseStats.get('Con') / 2 - 5));
    this.character.currentHp = Math.floor(this.selectedClass['hit_die'] + (this.baseStats.get('Con') / 2 - 5));
    this.character.languages = this.characterForm.get('languages').value;
    this.character.proficiencies = this.proficiencies; // clean up chip list styling and limit choices
    this.character.classes = classMap;
    this.character.stats = statMap;
    this.character.savingThrows = savingThrowsMap;
  }

  setInventoryInfo() {
    this.inventory = {
      id: null,
      characterName: this.characterForm.get('characterName').value,
      userid: this.userid,
      platinum: 0,
      gold: 10,
      silver: 0,
      copper: 0,
      items: this.items
    };
  }

  setSpellsAndAbilitiesInfo() {
    this.spellsAndAbilities = {
      id: null,
      characterName: this.characterForm.get('characterName').value,
      userid: this.userid,
      spellCastingAbility: this.castingAbility,
      spellSave: (10 + Math.floor(this.baseStats.get(this.castingAbility) / 2 - 5)).toString(),
      spellAttackBonus: (Math.floor(this.baseStats.get(this.castingAbility) / 2 - 5) + 2).toString(),
      spellSlots: [{level: 1, slotTotals: {used: 0, total: this.spellSlots}}],
      spells: this.spells,
      abilities: this.abilities
    };
  }
}


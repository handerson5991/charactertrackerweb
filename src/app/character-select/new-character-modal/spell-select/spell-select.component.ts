import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SpellsAndAbilitiesService} from '../../../landing/spells-and-abilities/spells-and-abilities.service';
import {Spell} from '../../../shared/dtos/spell';
import {AuthService} from '../../../auth/auth.service';
import {DirectoryService} from '../../../shared/services/directory.service';

@Component({
  selector: 'app-spell-select',
  templateUrl: './spell-select.component.html',
  styleUrls: ['./spell-select.component.css']
})
export class SpellSelectComponent implements OnInit {
  @Input() cantrips: number;
  @Input() spellsKnown: number;
  @Input() _class: string;
  @Output() spellsEmitter: EventEmitter<Spell[]> = new EventEmitter(null);
  addedSpells = [];
  addedCantrips = [];
  availableSpells: Spell[];
  nullSpell = {
    name: null,
    level: null,
    school: null,
    type: null,
    classes: null,
    tags: null,
    components: null,
    description: null,
    casting_time: null,
    range: null,
    duration: null,
    ritual: null
  };

  constructor(private directoryService: DirectoryService) {
  }

  ngOnInit() {
    this.getSpells();
    for (let i = 0; i < this.cantrips; i++) {
      this.addedCantrips.push(this.nullSpell);
    }
    for (let i = 0; i < this.spellsKnown; i++) {
      this.addedSpells.push(this.nullSpell);
    }
  }

  getSpells() {
    const lvlArray = this.spellsKnown > 0 ? [0, 1] : [0];
    this.directoryService.getSpellsByClassAndLevel(this._class, lvlArray)
      .subscribe(results => {
        this.availableSpells = results;
      });
  }

  addSpell(event) {
    if (event.level === 0) {
      for (let i = 0; i < this.cantrips; i++) {
        if (!this.addedCantrips[i].name && !this.addedCantrips.includes(event)) {
          this.addedCantrips[i] = event;
          break;
        }
      }
    } else if (event.level === 1) {
      for (let i = 0; i < this.spellsKnown; i++) {
        if (!this.addedSpells[i].level && !this.addedSpells.includes(event)) {
          this.addedSpells[i] = event;
          break;
        }
      }
    }
    if (!this.addedSpells.includes(this.nullSpell) && !this.addedSpells.includes(this.nullSpell)) {
      this.saveSpells();
    }
  }

  saveSpells() {
    this.spellsEmitter.emit(this.addedCantrips.concat(this.addedSpells));
  }
}

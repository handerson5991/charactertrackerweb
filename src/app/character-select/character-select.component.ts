import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {User} from '../shared/dtos/user';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NewCharacterModalComponent} from './new-character-modal/new-character-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-character-select',
  templateUrl: './character-select.component.html',
  styleUrls: ['./character-select.component.css']
})
export class CharacterSelectComponent implements OnInit {
  user: User;

  constructor(private authService: AuthService, private modalService: NgbModal, private router: Router) {
  }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  addCharacter() {
    const newCharacterModal = this.modalService.open(NewCharacterModalComponent, {windowClass: 'mediumModal'});
    newCharacterModal.result.then(result => {
      if (result !== 'cancel') {
        this.user.characters.push(result);
        this.authService.updateUser(this.user).subscribe(() => {
        });
      }
    }).catch(() => {
    });
  }

  selectCharacter(character: string) {
    this.router.navigate(['/landing'], {queryParams: {character}}).then();
  }
}

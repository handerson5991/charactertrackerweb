import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {environment} from '../../environments/environment';
import {User} from '../shared/dtos/user';
import {snackbarWarningOptions} from '../shared/config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private router: Router, private snackbar: MatSnackBar) {
  }

  login(creds: LoginCredentials): Observable<any> {
    return this.http.get(`${environment.servicesUrl}/api/user/get?userid=${creds.userid}&password=${creds.password}`).pipe(
      map((resp: any) => {
      if (resp) {
        this.user.next(resp);
        this.router.navigateByUrl('/character-select').then();
        } else {
        this.snackbar.open('Invalid userid or password', '', snackbarWarningOptions);
        }
      }), catchError(error => throwError(error))
    );
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/login']).then();
    this.snackbar.open('Logged out', '', snackbarWarningOptions);
  }

  createAccount(creds: LoginCredentials) {
    return this.http.post(`${environment.servicesUrl}/api/user/add`,
      {userid: creds.userid, password: creds.password, characters: []}).pipe(
      map((resp: any) => {
        this.user.next(resp);
        this.router.navigateByUrl('/character-select').then();
        return this.user.getValue();
      }), catchError(error => throwError(error))
    );
  }

  updateUser(user: User) {
    return this.http.put(`${environment.servicesUrl}/api/user/update/${user.userid}`, user.characters)
      .pipe(map(() => {
          this.user.next(user);
        }), catchError(error => throwError(error))
      );
  }
}

export interface LoginCredentials {
  userid: string;
  password: string;
}

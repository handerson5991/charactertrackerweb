import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (next.routeConfig) {
      const user = this.authService.user.value;
      if (user) {
        if (next.routeConfig.path === 'landing' && user.characters.length === 0) {
          this.router.navigate(['/login']).then();
          return false;
        } else {
          return true;
        }
      }
    }
    this.router.navigate(['/login']).then();
    return false;
  }
}

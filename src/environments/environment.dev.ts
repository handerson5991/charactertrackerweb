export const environment = {
  production: false,
  servicesUrl: 'http://charactertracker.us-west-1.elasticbeanstalk.com',
  dndApiUrl: 'https://www.dnd5eapi.co/api/',
  roll20Url: 'https://app.roll20.net/compendium/dnd5e'
};
